<?php

/**
 * Implements hook_views_default_views().
 */
function sms_altiria_log_views_default_views() {

  $views = [];

  $view = new view();
  $view->name = 'sms_altiria_logs_overview';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'sms_altiria_log';
  $view->human_name = 'SMS altiria logs overview';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'SMS altiria logs overview';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'más';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer smsframework';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Elementos por página';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Todos -';
  $handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Desplazamiento';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« primera';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ anterior';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'siguiente ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'última »';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Campo: SMS altiria log entity: Alias */
  $handler->display->display_options['fields']['alias']['id'] = 'alias';
  $handler->display->display_options['fields']['alias']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['alias']['field'] = 'alias';
  $handler->display->display_options['fields']['alias']['separator'] = '';
  /* Campo: SMS altiria log entity: Creado */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  /* Campo: SMS altiria log entity: Date the message was sent. */
  $handler->display->display_options['fields']['date']['id'] = 'date';
  $handler->display->display_options['fields']['date']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['date']['field'] = 'date';
  /* Campo: SMS altiria log entity: Keyword. */
  $handler->display->display_options['fields']['keyword']['id'] = 'keyword';
  $handler->display->display_options['fields']['keyword']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['keyword']['field'] = 'keyword';
  /* Campo: SMS altiria log entity: Message. */
  $handler->display->display_options['fields']['message']['id'] = 'message';
  $handler->display->display_options['fields']['message']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['message']['field'] = 'message';
  /* Campo: SMS altiria log entity: Phone number. */
  $handler->display->display_options['fields']['number']['id'] = 'number';
  $handler->display->display_options['fields']['number']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['number']['field'] = 'number';
  $handler->display->display_options['fields']['number']['separator'] = '';
  /* Campo: SMS altiria log entity: Primary key of the SMS altiria log */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  /* Campo: SMS altiria log entity: Provider. */
  $handler->display->display_options['fields']['provider']['id'] = 'provider';
  $handler->display->display_options['fields']['provider']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['provider']['field'] = 'provider';
  /* Campo: SMS altiria log entity: Short number. */
  $handler->display->display_options['fields']['shortnum']['id'] = 'shortnum';
  $handler->display->display_options['fields']['shortnum']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['shortnum']['field'] = 'shortnum';
  $handler->display->display_options['fields']['shortnum']['separator'] = '';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/smsframework/sms-altiria-log';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'SMS altiria logs';
  $handler->display->display_options['menu']['description'] = 'Logs from altiria';
  $handler->display->display_options['menu']['weight'] = '4';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Exportación de datos */
  $handler = $view->new_display('views_data_export', 'Exportación de datos', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Campo: SMS altiria log entity: Alias */
  $handler->display->display_options['fields']['alias']['id'] = 'alias';
  $handler->display->display_options['fields']['alias']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['alias']['field'] = 'alias';
  $handler->display->display_options['fields']['alias']['separator'] = '';
  /* Campo: SMS altiria log entity: Creado */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'stc_short_secondary';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Campo: SMS altiria log entity: Date the message was sent. */
  $handler->display->display_options['fields']['date']['id'] = 'date';
  $handler->display->display_options['fields']['date']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['date']['field'] = 'date';
  $handler->display->display_options['fields']['date']['date_format'] = 'stc_short_secondary';
  $handler->display->display_options['fields']['date']['second_date_format'] = 'long';
  /* Campo: SMS altiria log entity: Keyword. */
  $handler->display->display_options['fields']['keyword']['id'] = 'keyword';
  $handler->display->display_options['fields']['keyword']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['keyword']['field'] = 'keyword';
  /* Campo: SMS altiria log entity: Message. */
  $handler->display->display_options['fields']['message']['id'] = 'message';
  $handler->display->display_options['fields']['message']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['message']['field'] = 'message';
  /* Campo: SMS altiria log entity: Phone number. */
  $handler->display->display_options['fields']['number']['id'] = 'number';
  $handler->display->display_options['fields']['number']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['number']['field'] = 'number';
  $handler->display->display_options['fields']['number']['separator'] = '';
  /* Campo: SMS altiria log entity: Primary key of the SMS altiria log */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  /* Campo: SMS altiria log entity: Provider. */
  $handler->display->display_options['fields']['provider']['id'] = 'provider';
  $handler->display->display_options['fields']['provider']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['provider']['field'] = 'provider';
  /* Campo: SMS altiria log entity: Short number. */
  $handler->display->display_options['fields']['shortnum']['id'] = 'shortnum';
  $handler->display->display_options['fields']['shortnum']['table'] = 'sms_altiria_log';
  $handler->display->display_options['fields']['shortnum']['field'] = 'shortnum';
  $handler->display->display_options['fields']['shortnum']['separator'] = '';
  $handler->display->display_options['path'] = 'admin/smsframework/sms-altiria-log';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $translatables['sms_altiria_logs_overview'] = array(
    t('Master'),
    t('SMS altiria logs overview'),
    t('más'),
    t('Aplicar'),
    t('Reiniciar'),
    t('Ordenar por'),
    t('Asc'),
    t('Desc'),
    t('Elementos por página'),
    t('- Todos -'),
    t('Desplazamiento'),
    t('« primera'),
    t('‹ anterior'),
    t('siguiente ›'),
    t('última »'),
    t('Alias'),
    t('.'),
    t('Creado'),
    t('Date the message was sent.'),
    t('Keyword.'),
    t('Message.'),
    t('Phone number.'),
    t('Primary key of the SMS altiria log'),
    t(','),
    t('Provider.'),
    t('Short number.'),
    t('Page'),
    t('Exportación de datos'),
  );

  $views[$view->name] = $view;

  return $views;

}
