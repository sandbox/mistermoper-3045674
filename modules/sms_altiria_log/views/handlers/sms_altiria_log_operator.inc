<?php

/**
 * Field handler to get altiria operator.
 */
class sms_altiria_log_operator extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $operator = $this->get_value($values);
    $altiria_operators = sms_altiria_operator_list();
    return !empty($altiria_operators[$operator]) ? $altiria_operators[$operator] : NULL;
  }

}
