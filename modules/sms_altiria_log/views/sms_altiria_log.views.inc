<?php

/**
 * Implements hook_views_data_alter().
 */
function sms_altiria_log_views_data_alter(&$data) {
  $data['sms_altiria_log']['provider']['field']['handler'] = 'sms_altiria_log_operator';
}
