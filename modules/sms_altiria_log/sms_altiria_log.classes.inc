<?php

/**
 * @file
 * SMS altiria log classes.
 */

/**
 * CRUD controller for SMS altiria events.
 */
class SmsAltiriaLogEntityController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function create(array $values = array()) {
    $values += [
      'created' => REQUEST_TIME,
    ];
    return parent::create($values);
  }

}

/**
 * Register altiria SMS.
 */
class SmsAltiriaLogEntity extends Entity {

}
