<?php


/**
 * Implements hook_entity_property_info().
 */
function sms_altiria_log_entity_property_info() {
  $info = array();

  // Add meta-data about the basic commerce_order properties.
  $properties = &$info[SMS_ALTIRIA_LOG_ENTITY_TYPE]['properties'];
  
  $properties['sid'] = array(
    'type' => 'integer',
    'label' => t('Primary key of the SMS altiria log'),
    'description' => t('Primary key of the SMS altiria log.'),
    'schema field' => 'sid',
  );

  $properties['alias'] = array(
    'type' => 'integer',
    'label' => t('Alias'),
    'description' => t('Altiria identifier of this SMS.'),
    'schema field' => 'alias',
  );

  $properties['message'] = array(
    'type' => 'text',
    'label' => t('Message.'),
    'description' => t('User\'s email for this post.'),
    'schema field' => 'message',
  );

  $properties['keyword'] = array(
    'type' => 'text',
    'label' => t('Keyword.'),
    'description' => t('SMS keyword.'),
    'schema field' => 'keyword',
  );

  $properties['number'] = array(
    'type' => 'integer',
    'label' => t('Phone number.'),
    'description' => t('Phone number.'),
    'schema field' => 'number',
  );

  $properties['provider'] = array(
    'type' => 'integer',
    'label' => t('Provider.'),
    'description' => t('Provider.'),
    'schema field' => 'provider',
  );
  
  $properties['created'] = array(
    'type' => 'date',
    'label' => t('Created'),
    'description' => t('Date the message was saved in database.'),
    'schema field' => 'created',
  );

  $properties['date'] = array(
    'type' => 'date',
    'label' => t('Date the message was sent.'),
    'description' => t('Post creation unix time.'),
    'schema field' => 'date',
  );

  $properties['shortnum'] = array(
    'type' => 'integer',
    'label' => t('Short number.'),
    'description' => t('Short number.'),
    'schema field' => 'shortnum',
  );

  return $info;
}
